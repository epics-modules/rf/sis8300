/**
 * Struck 8300 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300AIChannel.cpp
 * @brief Implementation of sis8300 card analog input channel in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "ndsADIOChannel.h"
#include "ndsChannelStateMachine.h"
#include "ndsChannelStates.h"

#include "sis8300Device.h"
#include "sis8300AIChannelGroup.h"
#include "sis8300AIChannel.h"

#include "sis8300drv.h"


std::string sis8300AIChannel::PV_REASON_LINEAR_CONVERSION_FACTOR    = "LinearCnvFactor";
std::string sis8300AIChannel::PV_REASON_LINEAR_CONVERSION_OFFSET    = "LinearCnvOffset";
std::string sis8300AIChannel::PV_REASON_ATTENUATION                 = "Attenuation";
std::string sis8300AIChannel::PV_REASON_X_AXIS                      = "XAxis";
std::string sis8300AIChannel::PV_REASON_ENABLE_TRANSF               = "EnableTransf";


/**
 * @brief AI channel constructor.
 */

sis8300AIChannel::sis8300AIChannel() :  sis8300AIChannel(FSAMPLING_DEFAULT){}

sis8300AIChannel::sis8300AIChannel(epicsFloat64 FSampling) {

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&sis8300AIChannel::onLeaveProcessing, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
            boost::bind(&sis8300AIChannel::onEnterDisabled, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&sis8300AIChannel::onEnterError, this, _1, _2));

    _ChannelDataRaw = NULL;
    _ChannelData = NULL;
    _XAxis = NULL;
    _FSampling = FSampling;
    _AttenuationMin = 0;

    if (getChannelNumber() < SIS8300DRV_RTM_ATT_VM) {
        NDS_DBG("Update AttenuationMax");
        _AttenuationMax = SIS8300DRV_RTM_ATT_AI_MAX;
        _AttenuationStep = SIS8300DRV_RTM_ATT_AI_STEP_SIZE;

    } else if (getChannelNumber() == SIS8300DRV_RTM_ATT_VM) {
        _AttenuationMax = SIS8300DRV_RTM_ATT_VM_MAX;
        _AttenuationStep = SIS8300DRV_RTM_ATT_VM_STEP_SIZE;
    }

    /* task to take care of set attenuation */
    _setAttenuationTask = nds::ThreadTask::create(
            nds::TaskManager::generateName("setAttenuationTask"),
            epicsThreadGetStackSize(epicsThreadStackMedium),
            epicsThreadPriorityLow,
            boost::bind(&sis8300AIChannel::setAttenuationTask, this, _1));


}


sis8300AIChannel::~sis8300AIChannel() {
    free(_ChannelDataRaw);
    free(_ChannelData);
    free(_XAxis);
}


/**
 * @brief Registers handlers for interfacing with records. For more information,
 * refer to NDS documentation.
 */
ndsStatus sis8300AIChannel::registerHandlers(nds::PVContainers* pvContainers) {

    nds::ADIOChannel::registerHandlers(pvContainers);

    NDS_PV_REGISTER_FLOAT64(
            sis8300AIChannel::PV_REASON_LINEAR_CONVERSION_FACTOR,
            &sis8300AIChannel::setLinearConversionFactor,
            &sis8300AIChannel::getLinearConversionFactor,
            &_interruptIdLinearConversionFactor);

    /* Interface for conversion offset value modification */
    NDS_PV_REGISTER_FLOAT64(
            sis8300AIChannel::PV_REASON_LINEAR_CONVERSION_OFFSET,
            &sis8300AIChannel::setLinearConversionOffset,
            &sis8300AIChannel::getLinearConversionOffset,
            &_interruptIdLinearConversionOffset);

    NDS_PV_REGISTER_FLOAT64(
            sis8300AIChannel::PV_REASON_ATTENUATION,
            &sis8300AIChannel::setAttenuation,
            &sis8300AIChannel::getAttenuation,
            &_interruptIdAttenuation);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300AIChannel::PV_REASON_X_AXIS,
            NULL,
            &sis8300AIChannel::getXAxis,
            &_interruptIdXAxis);

    NDS_PV_REGISTER_INT32(
           sis8300AIChannel::PV_REASON_ENABLE_TRANSF,
           &sis8300AIChannel::setEnableTransf,
           &sis8300AIChannel::getEnableTransf,
           &_interruptIdEnableTransf
     );

    return ndsSuccess;
}

bool sis8300AIChannel::isCalibrated() {
    return 0;
}

double sis8300AIChannel::calibrate(double value) {
    return value;
}

/**
 * @brief Enable or disable channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Disable (0) or enable (non-0) the channel.
 *
 * @retval ndsSuccess Channel enabled/disabled successfully.
 * @retval ndsError Operation not allowed.
 *
 * All enabled channel will be included in the acquisition when the AI channel
 * group goes to PROCESSING state. Enabling/disabling channels is not allowed
 * if the channel group is processing.
 */
ndsStatus sis8300AIChannel::setEnabled(asynUser* pasynUser, epicsInt32 value) {
    ndsStatus               statusSamplesCount;
    sis8300AIChannelGroup   *cg;

    NDS_TRC("%s", __func__);

    cg = dynamic_cast<sis8300AIChannelGroup *>(getChannelGroup());

    cg->lock();

    if (value) {
        if (_isEnabled) {
            NDS_DBG("Channel %d already enabled.", getChannelNumber());
            cg->unlock();
            return ndsSuccess;
        }
        
        /* If the device is in a state with an open device descriptor check
         * if an additional channel can be enabled with the current
         * setting of number of samples, otherwise the check will be made when
         * the device opens the file descriptor. */
        if (_device->getCurrentState() != nds::DEVICE_STATE_OFF &&
                _device->getCurrentState() != nds::DEVICE_STATE_IOC_INIT) {
            statusSamplesCount = cg->checkSamplesConfig(-1, cg->getEnabledChannels() + 1, NULL);
            if (statusSamplesCount != ndsSuccess) {
                NDS_ERR("Number of samples too large for currently enabled channels.");
                cg->unlock();
                return ndsError;
            }
        }

    }

    _isEnabled = !!value;
    _EnabledChanged = 1;

    cg->unlock();

    NDS_DBG("Channel %d %s.", getChannelNumber(), value ? "enabled" : "disabled");

    return commitParameters();
}


/**
 * @brief Set number of samples to be acquired for the channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Number of samples.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Operation not allowed.
 *
 * The number of samples indicates how much data is expected for the channel at
 * the end of acquisition. This can only be set by the AI channel group.
 */
ndsStatus sis8300AIChannel::setSamplesCount(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    /* Only the channel group is allowed to set samples count and it calls
     * the setter with pasynUser=NULL. */
    if (pasynUser) {
        NDS_ERR("Setting number of samples must be done through the AI channel group.");
        return ndsError;
    }

    _SamplesCountCG = SIS8300NDS_ROUNDUP_TWOHEX(value);
    _SamplesCountChanged = 1;

    NDS_DBG("Setting channel %d number of samples to %d.", getChannelNumber(), _SamplesCountCG);

    return commitParameters();
}

/**
 * @brief Set linear conversion factor for the channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Linear conversion factor.
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300AIChannel::setLinearConversionFactor(asynUser* pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _LinearConversionFactor = value;
    _LinearConversionFactorChanged = 1;

    return commitParameters();
}


/**
 * @brief Get linear conversion factor for the channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Linear conversion factor.
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300AIChannel::getLinearConversionFactor(asynUser* pasynUser, epicsFloat64 *value) {
    NDS_TRC("%s", __func__);

    /* Prevent overwriting of VAL value at IOC init. */
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _LinearConversionFactor;
    return ndsSuccess;
}


/**
 * @brief Set linear conversion offset for the channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Linear conversion offset.
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300AIChannel::setLinearConversionOffset(asynUser* pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _LinearConversionOffset = value;
    _LinearConversionOffsetChanged = 1;

    return commitParameters();
}


/**
 * @brief Get linear conversion offset for the channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Linear conversion offset.
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300AIChannel::getLinearConversionOffset(asynUser* pasynUser, epicsFloat64 *value) {
    NDS_TRC("%s", __func__);

    /* Prevent overwriting of VAL value at IOC init. */
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _LinearConversionOffset;
    return ndsSuccess;
}

/**
 * @brief Set attenuation factor for the channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Linear conversion offset.
 *
 * @retval ndsSuccess Successfully set.
 * @retval ndsError Argument out of bounds (bounds depend on RTM type) or
 * not supported for this channel.
 */
ndsStatus sis8300AIChannel::setAttenuation(asynUser* pasynUser, epicsFloat64 value) {
    ndsStatus   status, statusCommit;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;
    if (getChannelNumber() > SIS8300DRV_RTM_ATT_VM) {
        NDS_ERR("Attenuation not supported on this channel.");
        return ndsError;
    }

    if (value < _AttenuationMin) {
        NDS_ERR("Attenuation %lf for channel %d out of range.", value, getChannelNumber());
        value = _AttenuationMin;
        status = ndsError;
    }

    if (value > _AttenuationMax) {
        NDS_ERR("Attenuation %lf for channel %d out of range.", value, getChannelNumber());
        value = _AttenuationMax;
        status = ndsError;
    }

    _Attenuation = value;
    _AttenuationChanged = 1;
    
    NDS_DBG("Setting attenuation for channel %d to %lf.", getChannelNumber(), value);

    statusCommit = commitParameters();
    if (statusCommit != ndsSuccess) {
        status = ndsError;
    }
    return status;
}

/**
 * @brief Get linear conversion offset for the channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Linear conversion offset.
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300AIChannel::getAttenuation(asynUser* pasynUser, epicsFloat64 *value) {
    NDS_TRC("%s", __func__);

    /* Prevent overwriting of VAL value at IOC init. */
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _Attenuation;
    return ndsSuccess;
}

/**
 * @brief Reet attenuation factor for the channel.
 *
 * @retval ndsSuccess Successfully reset attenuation if applicable for channel.
 * @retval ndsError Error commiting parameters to hardware.
 */
ndsStatus sis8300AIChannel::resetAttenuation() {
    NDS_TRC("%s", __func__);

    if (getChannelNumber() > SIS8300DRV_RTM_ATT_VM) {
        NDS_WRN("Attenuation not supported on this channel.");
        return ndsSuccess;
    }

    NDS_DBG("Resetting attenuation for channel %d to %lf.", 
            getChannelNumber(), _AttenuationMax);

    _Attenuation = _AttenuationMax;
    _AttenuationChanged = 1;

    return commitParameters();
}


/**
 * @brief Read single AI channel value.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value AI channel value in volts.
 *
 * @retval ndsSuccess Value read successfully.
 * @retval ndsError Card configuration failed, error reading the value
 * from hardware or the operation is not permitted.
 *
 * Configures the card for a software triggered acquisition, the minimum
 * number of samples and no pretrigger samples. The sampling frequency is left
 * as it is since it has impact on the attenuation factor.
 * This is used for single value reads from analog channels.
 * This operation is not allowed while the channel group is processing.
 */
ndsStatus sis8300AIChannel::getValueFloat64(asynUser* pasynUser, epicsFloat64 *value) {
    nds::ChannelGroup       *cg;
    int                     status;
    epicsUInt16             dataBuffer[SIS8300DRV_BLOCK_SAMPLES];
    sis8300drv_clk_src      clkSrc;
    sis8300drv_clk_div      clkDiv;
    unsigned                nsamples, npretrig, channel_mask;

    NDS_TRC("%s", __func__);

    cg = getChannelGroup();

    cg->lock();

    if (_device->getCurrentState() != nds::DEVICE_STATE_ON) {
        NDS_ERR("Channel value can be read only when device is in ON state.");
        cg->unlock();
        return ndsError;
    }

    if (cg->getCurrentState() != nds::CHANNEL_STATE_DISABLED) {
        NDS_ERR("Channel value can be read only when AI channel group is in DISABLED state.");
        cg->unlock();
        return ndsError;
    }

    /* Readout settings from the board so that they can be restored afterwards. */
    status = sis8300drv_get_clock_source(_DeviceUser, &clkSrc);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_get_clock_source", status, cg);

    status = sis8300drv_get_clock_divider(_DeviceUser, &clkDiv);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_get_clock_divider", status, cg);

    status = sis8300drv_get_nsamples(_DeviceUser, &nsamples);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_get_nsamples", status, cg);

    status = sis8300drv_get_npretrig(_DeviceUser, &npretrig);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_get_npretrig", status, cg);

    status = sis8300drv_get_channel_mask(_DeviceUser, &channel_mask);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_get_channel_mask", status, cg);


    /* Set temporary settings for single value read. */
    if (clkSrc == clk_src_internal && clkDiv == SIS8300DRV_CLKDIV_MIN) {
        status = sis8300drv_set_clock_divider(_DeviceUser, SIS8300DRV_CLKDIV_INT_MIN);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_clock_divider", status, cg);
    }

    status = sis8300drv_set_nsamples(_DeviceUser, SIS8300DRV_BLOCK_SAMPLES);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_nsamples", status, cg);

    status = sis8300drv_set_npretrig(_DeviceUser, 0);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_npretrig", status, cg);

    status = sis8300drv_set_channel_mask(_DeviceUser, 0x3FF);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_channel_mask", status, cg);


    /* Perform single value read. */
    status = sis8300drv_set_trigger_source(_DeviceUser, trg_src_soft);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_trigger_source", status, cg);

    status = sis8300drv_arm_device(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT_UNLOCK(" sis8300drv_set_trigger_source", status, cg);

    status = sis8300drv_wait_acq_end(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_arm_device", status, cg);

    status = sis8300drv_read_ai(_DeviceUser, getChannelNumber(), dataBuffer);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_read_ai", status, cg);


    /* Restore settings. */
    status = sis8300drv_set_clock_divider(_DeviceUser, clkDiv);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_clock_divider", status, cg);

    status = sis8300drv_set_nsamples(_DeviceUser, nsamples);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_nsamples", status, cg);

    status = sis8300drv_set_npretrig(_DeviceUser, npretrig);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_npretrig", status, cg);

    status = sis8300drv_set_channel_mask(_DeviceUser, channel_mask);
    SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300drv_set_channel_mask", status, cg);


    cg->unlock();

    *value = (epicsFloat64)dataBuffer[0]  * _LinearConversionFactor + _LinearConversionOffset;

    NDS_DBG("Channel %d read value: raw=%u egu=%lfV.", getChannelNumber(), dataBuffer[0], *value);

    return ndsSuccess;
}


/**
 * @brief Get the X-Axis for this channel
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] outArray    Waveform with X axis       
 * @param [out] nIn         Number of elements read
 * 
 * @return always  ndsSuccess
 *
 * Return the X axis array and its size.
 * */

ndsStatus sis8300AIChannel::getXAxis(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn) {
   NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }
   
    if (nelem < (size_t)_SamplesCountUsed)
        return ndsError;

    outArray = _XAxis;
    *nIn = _SamplesCountUsed;

    return ndsSuccess;
}


/**
 * @brief State handler for transition from PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 * Acquisition can only be started if channel group is in DISABLED state
 * @retval ndsSuccess Successful transition.
 *
 * Check if the acquisition finished successfully. If so then retrieve the
 * raw buffer from the device and convert to volts. For obtaining the raw values
 * the second half of the voltage buffer is used.
 */
ndsStatus sis8300AIChannel::onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    sis8300AIChannelGroup   *cg;
    int                     status, iter;
    epicsInt32              nsamples;

    NDS_TRC("%s", __func__);

    if (!_EnableTransf) { //The data transfer is disabled
        doCallbacksFloat32Array(NULL, 0, _interruptIdBufferFloat32);
        return ndsSuccess;
    }

    nsamples = _SamplesCountUsed;

    /* If acquisition didn't finish completely do nothing. */
    cg = dynamic_cast<sis8300AIChannelGroup *>(getChannelGroup());
    if (!cg->isDaqFinished()) {
        NDS_DBG("Daq not completed channel %d skipping buffer update.", getChannelNumber());
        return ndsSuccess;
    }

    NDS_DBG("Calling sis8300drv_read_ai for channel %d", getChannelNumber());
    status = sis8300drv_read_ai(_DeviceUser, getChannelNumber(), _ChannelDataRaw);
    SIS8300NDS_STATUS_CHECK("sis8300drv_read_ai", status);

    for (iter = 0; iter < nsamples; iter++) {
        _ChannelData[iter] = (epicsFloat64)(((epicsInt16)_ChannelDataRaw[iter]) / _convFact)* _LinearConversionFactor + _LinearConversionOffset;
    }

    doCallbacksFloat64Array(_ChannelData, (size_t)nsamples, _interruptIdBufferFloat64);

    return ndsSuccess;
}


/**
 * @brief State handler for transition to DISABLED.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Commit all parameters to hardware.
 */
ndsStatus sis8300AIChannel::onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to) {
    ndsStatus   statusCommit;

    NDS_TRC("%s", __func__);

    statusCommit = commitParameters();
    if (statusCommit != ndsSuccess) {
        return ndsError;
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the AI channel group to ERROR state.
 */
ndsStatus sis8300AIChannel::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    nds::ChannelGroup   *cg;

    NDS_TRC("%s", __func__);

    cg = getChannelGroup();

    /* NDS channel group doesn't handle being put into ERROR multiple times.
     * This should be fixed, multiple requests for ERROR should just do nothing. */
    if (cg->getCurrentState() != nds::CHANNEL_STATE_ERROR) {
        NDS_DBG("Putting AI channel group into ERROR state.");
        cg->error();
    }

    return ndsSuccess;
}


/**
 * @brief Message handler for START message.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Received message (always START in our case).
 *
 * @retval ndsError This is never allowed.
 *
 * Starting an acquisition should only be done through the AI channel group.
 */
ndsStatus sis8300AIChannel::handleStartMsg(asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("AI channel cannot be started manually.");
    return ndsError;
}


/**
 * @brief Message handler for STOP message.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Received message (always STOP in our case).
 *
 * @retval ndsError This is never allowed.
 *
 * Stopping an acquisition should only be done through the AI channel group.
 */
ndsStatus sis8300AIChannel::handleStopMsg(asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("AI channel cannot be stopped manually.");
    return ndsError;
}


/**
 * @brief Commit parameters that gave changed to hardware.
 *
 * @retval ndsSuccess Parameters committed successfully or writing to hardware
 * was not possible at this time.
 * @retval ndsError Error while trying to write parameters to hardware.
 *
 * For each parameter that has changed since this function was called last
 * the appropriate board settings are set. Also for each parameter that is
 * actually written to hardware the appropriate readback is updated.
 * This results in readbacks being updated at times when the appropriate
 * settings have been actually written to hardware.
 *
 * This function does nothing if called when the AI channel group is in PROCESSING
 * state or if the device is not in a state with an open file descriptor.
 */
ndsStatus sis8300AIChannel::commitParameters() {
    int                 status;
    unsigned            channel_mask;
    unsigned            nsamples_raw;   /* Number of samples read direct from digitiser memory */
    unsigned            nsamples;       /* Number of samples publised to PV*/
    unsigned            iter;
    nds::ChannelGroup   *cg;

    NDS_TRC("%s", __func__);

    //Any change can be done when device is off
    if (_device->getCurrentState() == nds::DEVICE_STATE_OFF)  {
        NDS_DBG("AI channel %d not committing parameters, because device is OFF.", getChannelNumber());
        return ndsSuccess;
    }

    //Attenuator can be changed on INIT and ON, so apply changes before the others
    //It will not apply when the state is RESETTING
    if (_AttenuationChanged && _device->getCurrentState() !=  nds::DEVICE_STATE_RESETTING) {
        if (getChannelNumber() > SIS8300DRV_RTM_ATT_VM){
            NDS_DBG("Attenuation cannot be set on the channel %d", getChannelNumber());
        }
        else {
            _setAttenuationTask->start();
            NDS_DBG("%s is in %s.", _setAttenuationTask->getName().c_str(), _setAttenuationTask->getStateStr().c_str());
        }

        _AttenuationChanged = 0;
    }

    cg = getChannelGroup();
    channel_mask = 0;

    if (cg->getCurrentState() != nds::CHANNEL_STATE_DISABLED ||
            _device->getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        NDS_DBG("AI channel %d not committing parameters.", getChannelNumber());
        return ndsSuccess;
    }

    if (_EnabledChanged) {
        doCallbacksInt32(_isEnabled, _interruptIdEnabled);
        _EnabledChanged = 0;

        status = sis8300drv_get_channel_mask(_DeviceUser, &channel_mask);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_get_channel_mask", status);
        NDS_DBG("sis8300drv_get_channel_mask returned %u", channel_mask);

        if (_isEnabled) {
            channel_mask |= (1 << getChannelNumber());
        } else {
            channel_mask &= ~(1 << getChannelNumber());
        }

        NDS_DBG("Calling sis8300drv_set_channel_mask with %u", channel_mask);
        status = sis8300drv_set_channel_mask(_DeviceUser, channel_mask);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_set_channel_mask", status);
    }

    if (_SamplesCountChanged) {
        /* Run-time reallocation of buffers based on user-specified sample count */
        nsamples_raw = _SamplesCountCG;
        _ChannelDataRaw = (epicsUInt16 *)realloc(_ChannelDataRaw, sizeof(epicsUInt16) * nsamples_raw);
        if (!_ChannelDataRaw) {
            NDS_ERR("Cannot allocate memory for channel %d raw data.", getChannelNumber());
            SIS8300NDS_MSGERR("Cannot allocate memory for channel raw data");
            error();
            return ndsError;
        }
        NDS_DBG("Channel %d allocated buffer for %u raw samples.", getChannelNumber(), nsamples_raw);

        _SamplesCount = _SamplesCountCG;
        _SamplesCount = _SamplesCount ? _SamplesCount : 1;
        nsamples = SIS8300NDS_ROUNDUP_HEX(_SamplesCount);

        _ChannelData = (epicsFloat64 *)realloc(_ChannelData, sizeof(epicsFloat64) * nsamples);
        if (!_ChannelData) {
            NDS_ERR("Cannot allocate memory for channel %d EGU data.", getChannelNumber());
            SIS8300NDS_MSGERR("Cannot allocate memory for channel EGU data");
            error();
            return ndsError;
        }
        NDS_DBG("Channel %d allocated buffer for %u EGU samples.", getChannelNumber(), nsamples);

        doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);
        _SamplesCountUsed = _SamplesCount;
        _SamplesCountChanged = 0;
        
        /* Update XAxis corresponding to _ChannelData
         * Note FSampling is considered constant after IOC initialisation.
         * There is no way for user to change the FSampling value after IOC initialisation. */

        _XAxis = (epicsFloat64 *) realloc(_XAxis, sizeof(epicsFloat64) * nsamples);
        if (!_XAxis) {
            NDS_ERR("Cannot allocate memory for channel %d X-Axis data.", getChannelNumber());
            SIS8300NDS_MSGERR("Cannot allocate mmeory for channel X-Axis");
            error();
            return ndsError;
        }
        NDS_DBG("Channel %d allocated buffer for %u X-Axis points.", getChannelNumber(), nsamples);

        /* Go ahead and populate XAxis at this point as we have all required info */
        for (iter = 0; iter < nsamples; iter++) {
                _XAxis[iter] = (epicsFloat64) (iter) * 1/(_FSampling * 1000);
        }
        // update PV
        doCallbacksFloat64Array(_XAxis, _SamplesCountUsed, _interruptIdXAxis);

    }

    if (_LinearConversionFactorChanged) {
        doCallbacksFloat64(_LinearConversionFactor, _interruptIdLinearConversionFactor);
        _LinearConversionFactorChanged = 0;
    }

    if (_LinearConversionOffsetChanged) {
        doCallbacksFloat64(_LinearConversionOffset, _interruptIdLinearConversionOffset);
        _LinearConversionOffsetChanged = 0;
    }

    return ndsSuccess;
}


/**
 * @brief Flag all parameters as changed.
 *
 * @retval ndsSuccess All parameters flagged as changed.
 *
 * All parameters of the AI channel are flagged as changed.
 */
ndsStatus sis8300AIChannel::markAllParametersChanged() {
    NDS_TRC("%s", __func__);

    _EnabledChanged = 1;
    _SamplesCountChanged = 1;
    _LinearConversionFactorChanged = 1;
    _LinearConversionOffsetChanged = 1;
    _AttenuationChanged = 1;

    return ndsSuccess;
}

/**
 * @brief Set device context for the channel.
 * @param [in] newDeviceUser Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context.
 */
ndsStatus sis8300AIChannel::setDeviceUser(sis8300drv_usr *newDeviceUser) {
    _DeviceUser = newDeviceUser;
    return ndsSuccess;
}

/**
 * @brief Enable the data transfer
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Enable/Disable
 * 
 * @return always  ndsSuccess
 *
 * Set a private variable tha enable or disable the data transfer
 * from memory. This doesn't change anything on firmware, but IOC
 * will log load de acquired data.
 */
ndsStatus sis8300AIChannel::setEnableTransf(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _EnableTransf = value;

    doCallbacksInt32(_EnableTransf, _interruptIdEnableTransf);
    return ndsSuccess;
}

/**
 * @brief Read if data transfer is Enable 
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Enable/Disable
 * 
 * @return ndsSuccess       Always success
 * 
 * This will return if the data transfer is enable or not
 */
ndsStatus sis8300AIChannel::getEnableTransf(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);

    *value = (epicsInt32) _EnableTransf;

    return ndsSuccess;
}



//Task to set attenuation
ndsStatus sis8300AIChannel::setAttenuationTask(nds::TaskServiceBase &service) {
    NDS_TRC("%s", __func__);

    int status;

    NDS_DBG("Entered Thread");

    NDS_DBG("Before set attenuator");
    status= sis8300drv_rtm_attenuator_set(_DeviceUser, (unsigned)getChannelNumber(), &_Attenuation);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_rtm_attenuator_set", status);
    NDS_DBG("Set attenuator for channel %d to  = %lf dB.",
            getChannelNumber(), _Attenuation);

    doCallbacksFloat64(_Attenuation, _interruptIdAttenuation);

    return ndsSuccess;
}
