/**
 * @file sis8300AIChannelGroup.h
 * @brief Header file defining the sis8300 card analog output channel group class.
 * @author kstrnisa
 * @date 20.8.2013
 */

#ifndef _sis8300AOChannelGroup_h
#define _sis8300AOChannelGroup_h

#include "ndsChannelGroup.h"

#include "sis8300drv.h"


/**
 * @brief sis8300 specific nds::ChannelGroup class that support common AO functionality.
 */
class sis8300AOChannelGroup : public nds::ChannelGroup {

public:
    sis8300AOChannelGroup(const std::string& name);
    virtual ~sis8300AOChannelGroup();

    ndsStatus setDeviceUser(sis8300drv_usr *newDeviceUser);

protected:
    sis8300drv_usr *_DeviceUser;    /**< User context for sis8300drv, set by NDS Device. */

    virtual ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300AOChannelGroup_h */
