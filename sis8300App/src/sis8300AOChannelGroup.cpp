/**
 * Struck 8300 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300AOChannelGroup.cpp
 * @brief Implementation of sis8300 card analog output channel group in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "ndsChannelGroup.h"
#include "ndsChannelStateMachine.h"

#include "sis8300Device.h"
#include "sis8300AOChannelGroup.h"
#include "sis8300AOChannel.h"

#include "sis8300drv.h"


/**
 * @brief AO ChannelGroup constructor.
 * @param [in] name Channel Group name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation.
 */
sis8300AOChannelGroup::sis8300AOChannelGroup(const std::string& name) :
        nds::ChannelGroup(name) {

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&sis8300AOChannelGroup::onEnterError, this, _1, _2));
}


sis8300AOChannelGroup::~sis8300AOChannelGroup() {}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the device to ERROR state.
 */
ndsStatus sis8300AOChannelGroup::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _device->error();

    return ndsSuccess;
}


/**
 * @brief Set device context for the channel group.
 * @param [in] newDeviceUser Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context and push the device context to all the channels.
 */
ndsStatus sis8300AOChannelGroup::setDeviceUser(sis8300drv_usr *newDeviceUser) {
    _DeviceUser = newDeviceUser;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        (dynamic_cast<sis8300AOChannel *>(iter->second))->setDeviceUser(newDeviceUser);
    }
    return ndsSuccess;
}
