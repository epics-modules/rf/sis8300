/**
 * @file sis8300AIChannel.h
 * @brief Header file defining the sis8300 card analog input channel class.
 * @author kstrnisa
 * @date 20.8.2013
 */

#ifndef _sis8300AIChannel_h
#define _sis8300AIChannel_h

#include "ndsADIOChannel.h"

#include "ndsManager.h"
#include "ndsTaskManager.h"
#include "ndsThreadTask.h"

//FSAMPLING is in MHz
#define FSAMPLING_DEFAULT 117.403333333

/**
 * @brief sis8300 specific nds::ADIOChannel class that supports AI channels.
 */
class sis8300AIChannel : public nds::ADIOChannel {

public:
    sis8300AIChannel();
    sis8300AIChannel(epicsFloat64 FSampling);
    virtual ~sis8300AIChannel();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    /* Parameters implemented from base class(es). */
    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setSamplesCount(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getValueFloat64(asynUser* pasynUser, epicsFloat64 *value);

    /* Custom parameters defined by sis8300AIChannel. */
    virtual ndsStatus setLinearConversionFactor(asynUser* pasynUser, epicsFloat64 value);
    virtual ndsStatus getLinearConversionFactor(asynUser* pasynUser, epicsFloat64 *value);
    virtual ndsStatus setLinearConversionOffset(asynUser* pasynUser, epicsFloat64 value);
    virtual ndsStatus getLinearConversionOffset(asynUser* pasynUser, epicsFloat64 *value);
    virtual ndsStatus getXAxis(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn); 
    virtual ndsStatus setEnableTransf(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus getEnableTransf(asynUser* pasynUser, epicsInt32 *value);

    virtual ndsStatus setAttenuation(asynUser* pasynUser, epicsFloat64 value);
    virtual ndsStatus getAttenuation(asynUser* pasynUser, epicsFloat64 *value);

    virtual ndsStatus commitParameters();
    virtual ndsStatus markAllParametersChanged();
    virtual ndsStatus resetAttenuation();

    ndsStatus setDeviceUser(sis8300drv_usr *newDeviceUser);
    
    virtual bool isCalibrated();
    virtual double calibrate(double value);

protected:
    sis8300drv_usr  *_DeviceUser;           /**< User context for sis8300drv, set by AI channel group. */
    epicsUInt16     *_ChannelDataRaw;       /**< Data buffer used both for data readout. */
    epicsFloat64    *_ChannelData;          /**< Data buffer used both for EGU data. */
    epicsInt32      _SamplesCountCG;        /**< Number of samples as set in the channel group. */
    epicsFloat64    *_XAxis;                /**< Data buffer used for XAxis (time), for data plotting. */
    epicsFloat64    _AttenuationMin;
    epicsFloat64    _AttenuationMax;
    epicsFloat64    _AttenuationStep;
    epicsFloat64    _FSampling;             /**< Setting of sampling frequency in MHz for the digitiser, this is not-changing after IOC has started. */
    epicsInt32      _EnableTransf;
    
    const epicsFloat64    _convFact = 1 << 15;    /** < Channel data is fixed point data of format Qm.n=1.15. */

    /* Custom parameters defined by sis8300AIChannel. */
    static std::string PV_REASON_LINEAR_CONVERSION_FACTOR;      /**< Asyn reason for conversion factor. */
    static std::string PV_REASON_LINEAR_CONVERSION_OFFSET;      /**< Asyn reason for conversion offset. */
    static std::string PV_REASON_ATTENUATION;                   /**< Asyn reason for RTM attenuation. */

    static std::string PV_REASON_X_AXIS;      /**< Asyn reason for X_AXIS. */
    static std::string PV_REASON_ENABLE_TRANSF;   /**< Asyn reason for enable transfer */

    epicsFloat64 _LinearConversionFactor;   /**< Conversion factor internal variable. */
    epicsFloat64 _LinearConversionOffset;   /**< Conversion offset internal variable. */
    epicsFloat64 _Attenuation;              /**< Attenuation on RTM. */

    int _interruptIdLinearConversionFactor; /**< Interrupt ID for conversion factor. */
    int _interruptIdLinearConversionOffset; /**< Interrupt ID for conversion offset. */
    int _interruptIdAttenuation;            /**< Interrupt ID for RTM attenuation. */
    int _interruptIdXAxis;                  /**< Interrupt ID for XAxis */
    int _interruptIdEnableTransf;          /**< Interrupt ID for Enable Transference*/

    /* Flags to signify whether a parameter has been changed. */
    epicsInt32      _EnabledChanged;
    epicsInt32      _SamplesCountChanged;
    epicsInt32      _LinearConversionFactorChanged;
    epicsInt32      _LinearConversionOffsetChanged;
    epicsInt32      _AttenuationChanged;

    epicsInt32      _SamplesCountUsed;

    virtual ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);

    virtual ndsStatus handleStartMsg(asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleStopMsg(asynUser *pasynUser, const nds::Message& value);

    nds::ThreadTask *_setAttenuationTask;              /**< NDS task/thread to set attenuation */
    ndsStatus setAttenuationTask(nds::TaskServiceBase &service);
};

#endif /* _sis8300AIChannel_h */
