/**
 * @file sis8300Device.h
 * @brief Header file defining the sis8300 card Device class.
 * @author kstrnisa
 * @date 20.8.2013
 */

#ifndef _sis8300Device_h
#define _sis8300Device_h

#include "ndsDevice.h"

#include "sis8300drv.h"


#define SIS8300NDS_HW_REVISION  2               /**< Hardware revision supported. */
#define SIS8300NDS_SW_VERSION   1.9             /**< Version of the device driver. */

#define SIS8300NDS_AICG_NAME    "ai"            /**< ChanelGroup name for analog input channels. */
#define SIS8300NDS_AOCG_NAME    "ao"            /**< ChanelGroup name for analog output channels. */
#define SIS8300NDS_FPCG_NAME    "frontpanel"    /**< ChanelGroup name for frontpanel trigger lines. */
#define SIS8300NDS_BPCG_NAME    "backplane"     /**< ChanelGroup name for backplane trigger lines. */
#define SIS8300NDS_REGCG_NAME   "register"      /**< ChanelGroup name for device register access. */

#define SIS8300NDS_STRING_BUFFER_SIZE   256     /**< Size of temporary buffer for numeric to string conversions. */

/** Send an error message to the MSGR record. */
#define SIS8300NDS_MSGERR(text)                                                 \
        do {                                                                    \
            nds::Message message;                                               \
            message.messageType = "ERROR";                                      \
            message.insert("TEXT", (text));                                     \
            doCallbacksMessage(message);                                        \
        } while (0)

/** Send an error message to the MSGR record containing the name of the library
 * function, the error code and human readable error string. */
#define SIS8300NDS_STATUS_MSGERR(func, status)                                  \
        do {                                                                    \
            nds::Message message;                                               \
            char buffer[SIS8300NDS_STRING_BUFFER_SIZE];                         \
            message.messageType = "ERROR";                                      \
            snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%d", status);  \
            message.insert("CODE", buffer);                                     \
            snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1,                 \
                    "%s : %s (%d)", func, sis8300drv_strerror(status), status); \
            message.insert("TEXT", buffer);                                     \
            doCallbacksMessage(message);                                        \
        } while (0)

/** Status check helper that prints error message from userspace library,
 * sends a message to the MSGR record and returns ndsError. */
#define SIS8300NDS_STATUS_CHECK(func, status)                                   \
        do {                                                                    \
            if ((status) != status_success) {                                   \
                NDS_ERR("%s : %s (%d).",                                        \
                        (func), sis8300drv_strerror(status), (status));         \
                SIS8300NDS_STATUS_MSGERR(func, status);                         \
                return ndsError;                                                \
            }                                                                   \
        } while (0)
        
/** Status check helper that prints error message from userspace library, 
 * unlocks the channel group, sends a message to the MSGR record
 * and returns ndsError.. */
#define SIS8300NDS_STATUS_CHECK_UNLOCK(func, status, locked)                    \
        do {                                                                    \
            if ((status) != status_success) {                                   \
                NDS_ERR("%s : %s (%d).",                                        \
                        (func), sis8300drv_strerror(status), (status));         \
                SIS8300NDS_STATUS_MSGERR(func, status);                         \
                (locked)->unlock();                                             \
                return ndsError;                                                \
            }                                                                   \
        } while (0)

/** Status check helper that prints error message from userspace library,
 * puts the object into ERROR state, sends a message to the MSGR record
 * and returns ndsError.*/
#define SIS8300NDS_STATUS_ASSERT(func, status)                                  \
        do {                                                                    \
            if ((status) != status_success) {                                   \
                NDS_ERR("%s : %s (%d).",                                        \
                        (func), sis8300drv_strerror(status), (status));         \
                SIS8300NDS_STATUS_MSGERR(func, status);                         \
                error();                                                        \
                return ndsError;                                                \
            }                                                                   \
        } while (0)

/** Status check helper that prints error message from userspace library,
 * puts the object into ERROR state, unlocks the channel group,
 * sends a message to the MSGR record and returns ndsError. */
#define SIS8300NDS_STATUS_ASSERT_UNLOCK(func, status, locked)                   \
        do {                                                                    \
            if ((status) != status_success) {                                   \
                NDS_ERR("%s : %s (%d).",                                        \
                        (func), sis8300drv_strerror(status), (status));         \
                SIS8300NDS_STATUS_MSGERR(func, status);                         \
                error();                                                        \
                (locked)->unlock();                                             \
                return ndsError;                                                \
            }                                                                   \
        } while (0)

/** Round a number up to the next multiple of 2 (if it already is it stays unmodified). */
#define SIS8300NDS_ROUNDUP_TWO(val) (((unsigned)(val) + 0x1) & ~0x1)

/** Round a number up to the next multiple of 16 (if it already is it stays unmodified). */
#define SIS8300NDS_ROUNDUP_HEX(val) (((unsigned)(val) + 0xF) & ~0xF)

/** Round a number down to the next multiple of 16 (if it already is it stays unmodified). */
#define SIS8300NDS_ROUNDDOWN_HEX(val) ((unsigned)(val) & ~0xF)

/** Round a number up to the next multiple of 16 (if it already is it stays unmodified). */
#define SIS8300NDS_ROUNDUP_TWOHEX(val) ((unsigned)(val + 0x1F) & ~0x1F)

/** Round a number down to the next multiple of 32 (if it already is it stays unmodified). */
#define SIS8300NDS_ROUNDDOWN_TWOHEX(val) ((unsigned)(val) & ~0x1F)


/**
 * @brief sis8300 specific nds::Device class.
 */
class sis8300Device : public nds::Device {

public:
    sis8300Device(const std::string& name);
    virtual ~sis8300Device();
    
    virtual ndsStatus createStructure(const char* portName, const char* params);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus setRTMType(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getRTMType(asynUser *pasynUSer, epicsInt32 *value);
    virtual ndsStatus setVMOutputEnabled(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getVMOutputEnabled(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setVM_DC_Offset_I(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setVM_DC_Offset_Q(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getRTMADCTemp(asynUser *pasynUSer, epicsInt32 *value);
    virtual ndsStatus getRTMLOLevel(asynUser *pasynUSer, epicsInt32 *value);
    virtual ndsStatus getRTMLOMeterTemp(asynUser *pasynUSer, epicsInt32 *value);
    virtual ndsStatus getRTMVMOutLevel(asynUser *pasynUSer, epicsInt32 *value);
    virtual ndsStatus getRTMVMOutMeterTemp(asynUser *pasynUSer, epicsInt32 *value);

	virtual ndsStatus restoreAllValues();

protected:
    sis8300drv_usr  *_DeviceUser;        /**< User context for sis8300drv. */

    static std::string PV_REASON_RTM_TYPE;
    static std::string PV_REASON_VM_OUTPUT_ENABLE;
    static std::string PV_REASON_VM_DC_OFFSET_I;
    static std::string PV_REASON_VM_DC_OFFSET_Q;
    static std::string PV_REASON_RTM_ADC_TEMP;
    static std::string PV_REASON_RTM_LO_LEVEL;
    static std::string PV_REASON_RTM_LO_METER_TEMP;
    static std::string PV_REASON_RTM_VMOUT_LEVEL;
    static std::string PV_REASON_RTM_VMOUT_METER_TEMP;
    static std::string PV_REASON_MAIN_INSTANCE;

    int _interruptIdRTMType;
    int _interruptIdVMOutputEnable;
    int _interruptIdVM_DC_Offset_I;
    int _interruptIdVM_DC_Offset_Q;
    int _interruptIdRTMADCTemp;
    int _interruptIdRTMLOLevel;
    int _interruptIdRTMLOMeterTemp;
    int _interruptIdRTMVMOutLevel;
    int _interruptIdRTMVMOutMeterTemp;

    epicsInt32 _RTMType;
    epicsInt32 _VMOutputEnable;
    epicsInt32 _VM_DC_Offset_I;
    epicsInt32 _VM_DC_Offset_Q;
    epicsInt32 _MainInstance;

    virtual ndsStatus registerAIChannels();
    virtual ndsStatus registerAOChannels();
    virtual ndsStatus registerFrontTriggerChannels();
    virtual ndsStatus registerBackTriggerChannels();
    virtual ndsStatus registerRegisterChannels();

    virtual ndsStatus onSwitchInit(nds::DeviceStates from, nds::DeviceStates to);
    virtual ndsStatus onEnterInit(nds::DeviceStates from, nds::DeviceStates to);
    virtual ndsStatus fastInit(nds::DeviceStates from, nds::DeviceStates to);
    virtual ndsStatus onEnterOffState(nds::DeviceStates from, nds::DeviceStates to);
};

#endif /* _sis8300Device_h */
