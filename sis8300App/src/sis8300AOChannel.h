/**
 * @file sis8300AOChannel.h
 * @brief Header file defining the sis8300 card analog output channel class.
 * @author kstrnisa
 * @date 20.8.2013
 */

#ifndef _sis8300AOChannel_h
#define _sis8300AOChannel_h

#include <ndsADIOChannel.h>

/**
 * @brief sis8300 specific nds::ADIOChannel class that supports AO channels.
 */
class sis8300AOChannel : public nds::ADIOChannel {

public:
    sis8300AOChannel();
    virtual ~sis8300AOChannel();

    virtual ndsStatus setValueFloat64(asynUser* pasynUser, epicsFloat64 value);
    virtual ndsStatus getValueFloat64(asynUser* pasynUser, epicsFloat64 *value);

    ndsStatus setDeviceUser(sis8300drv_usr *newDeviceUser);

protected:
    sis8300drv_usr *_DeviceUser;    /**< User context for sis8300drv, set by AO channel group. */

    virtual ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300AOChannel_h */
