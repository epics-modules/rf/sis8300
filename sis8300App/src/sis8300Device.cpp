/**
 * Struck 8300 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300Device.cpp
 * @brief Implementation of sis8300 card Device in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include <string>

#include "ndsDeviceStates.h"
#include "ndsManager.h"
#include "ndsMessage.h"
#include "ndsChannelGroup.h"
#include "ndsAutoChannelGroup.h"
#include "ndsChannel.h"
#include "ndsDevice.h"

#include "sis8300Device.h"
#include "sis8300AIChannelGroup.h"
#include "sis8300AIChannel.h"
#include "sis8300AOChannelGroup.h"
#include "sis8300AOChannel.h"
#include "sis8300RegisterChannelGroup.h"
#include "sis8300RegisterChannel.h"

#include "sis8300drv.h"


std::string sis8300Device::PV_REASON_RTM_TYPE           = "RTMType";
std::string sis8300Device::PV_REASON_VM_OUTPUT_ENABLE   = "VMOutputEnable";
std::string sis8300Device::PV_REASON_VM_DC_OFFSET_I     = "OffsetI";
std::string sis8300Device::PV_REASON_VM_DC_OFFSET_Q     = "OffsetQ";
std::string sis8300Device::PV_REASON_RTM_ADC_TEMP         = "RTMADCTemp";
std::string sis8300Device::PV_REASON_RTM_LO_LEVEL         = "RTMLOLevel";
std::string sis8300Device::PV_REASON_RTM_LO_METER_TEMP    = "RTMLOMeterTemp";
std::string sis8300Device::PV_REASON_RTM_VMOUT_LEVEL      = "RTMVMOutLevel";
std::string sis8300Device::PV_REASON_RTM_VMOUT_METER_TEMP = "RTMVMOutMeterTemp";


nds::RegisterDriver<sis8300Device> exportedDevice("sis8300");


/**
 * @brief Device constructor.
 * @param [in] name Device name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation.
 */
sis8300Device::sis8300Device(const std::string& name) :
            nds::Device(name) {

    registerOnRequestStateHandler(nds::DEVICE_STATE_OFF, nds::DEVICE_STATE_INIT,
            boost::bind(&sis8300Device::onSwitchInit, this, _1, _2));

    registerOnEnterStateHandler(nds::DEVICE_STATE_INIT,
            boost::bind(&sis8300Device::onEnterInit, this, _1, _2));

    registerOnEnterStateHandler(nds::DEVICE_STATE_OFF,
            boost::bind(&sis8300Device::onEnterOffState, this, _1, _2));

    /* replace this, so that children can disable fast init if necessary. */
    pushbackOnEnterStateHandler(nds::DEVICE_STATE_INIT,
            boost::bind(&sis8300Device::fastInit, this, _1, _2));

    _RTMType = rtm_none;

	_VM_DC_Offset_I = 0;
	_VM_DC_Offset_Q = 0;
	_MainInstance = 0; // By default the instance is secondary
}


sis8300Device::~sis8300Device() {}


/**
 * @brief Registers handlers for interfacing with records. For more information,
 * refer to NDS documentation.
 */
ndsStatus sis8300Device::registerHandlers(nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_RTM_TYPE,
            &sis8300Device::setRTMType,
            &sis8300Device::getRTMType,
            &_interruptIdRTMType);

    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_VM_OUTPUT_ENABLE,
            &sis8300Device::setVMOutputEnabled,
            &sis8300Device::getVMOutputEnabled,
            &_interruptIdVMOutputEnable);

    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_VM_DC_OFFSET_I,
            &sis8300Device::setVM_DC_Offset_I,
            &sis8300Device::getInt32,
            &_interruptIdVM_DC_Offset_I);
    
    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_VM_DC_OFFSET_Q,
            &sis8300Device::setVM_DC_Offset_Q,
            &sis8300Device::getInt32,
            &_interruptIdVM_DC_Offset_Q);

    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_RTM_ADC_TEMP,
            NULL,
            &sis8300Device::getRTMADCTemp,
            &_interruptIdRTMADCTemp);

    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_RTM_LO_LEVEL,
            NULL,
            &sis8300Device::getRTMLOLevel,
            &_interruptIdRTMLOLevel);

    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_RTM_LO_METER_TEMP,
            NULL,
            &sis8300Device::getRTMLOMeterTemp,
            &_interruptIdRTMLOMeterTemp);

    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_RTM_VMOUT_LEVEL,
            NULL,
            &sis8300Device::getRTMVMOutLevel,
            &_interruptIdRTMVMOutLevel);

    NDS_PV_REGISTER_INT32(
            sis8300Device::PV_REASON_RTM_VMOUT_METER_TEMP,
            NULL,
            &sis8300Device::getRTMVMOutMeterTemp,
            &_interruptIdRTMVMOutMeterTemp);

    return nds::Device::registerHandlers(pvContainers);
}


/**
 * @brief Create driver structures.
 * @param [in] portName AsynPort name.
 * @param [in] params Refer to NDS documentation.
 *
 * @retval ndsSuccess createStructure always returns success.
 *
 * AI, AO and external trigger channel groups (for frontpanel and backplane trigger lines)
 * are registered and channels are registered to corresponding channel groups.
 * 
 * This is achieved by calling the following virtual functions:
 *  - #registerAIChannels
 *  - #registerAOChannels
 *  - #registerFrontTriggerChannels
 *  - #registerBackTriggerChannels
 *  - #registerRegisterChannels
 * 
 * In case custom channel groups and/or channels are needed these functions should
 * be overridden in the class that extends this one.
 * 
 * This function also allocate space for #_DeviceUser and copies the value of the
 * FILE parameter into it.
 */
ndsStatus sis8300Device::createStructure(const char* portName, const char* params) {
    ndsStatus           status;

    NDS_TRC("%s", __func__);

    _DeviceUser = (sis8300drv_usr *)calloc(1, sizeof(sis8300drv_usr));
    _DeviceUser->file = getStrParam("FILE", "").c_str();

    status = registerAIChannels();
    if (status != ndsSuccess) {
        return ndsSuccess;
    }

    status = registerAOChannels();
    if (status != ndsSuccess) {
        return ndsSuccess;
    }

    status = registerFrontTriggerChannels();
    if (status != ndsSuccess) {
        return ndsSuccess;
    }

    status = registerBackTriggerChannels();
    if (status != ndsSuccess) {
        return ndsSuccess;
    }

    status = registerRegisterChannels();
    if (status != ndsSuccess) {
        return ndsSuccess;
    }

    return ndsSuccess;
}


/**
 * @brief Register analog input channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered sucessfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registeres a #sis8300AIChannelGroup named #SIS8300NDS_AICG_NAME
 * and then registeres #SIS8300DRV_NUM_AI_CHANNELS times #sis8300AIChannel
 * with the channel group.
 */
ndsStatus sis8300Device::registerAIChannels() {
    int                     iter;
    ndsStatus               status;
    sis8300AIChannelGroup   *cgAI;

    cgAI = new sis8300AIChannelGroup(SIS8300NDS_AICG_NAME);
    status = registerChannelGroup(cgAI);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300NDS_AICG_NAME);
        return ndsError;
    }

    for (iter = 0; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {
        status = cgAI->registerChannel(new sis8300AIChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", SIS8300NDS_AICG_NAME, iter);
            return ndsError;
        }
    }

    cgAI->setDeviceUser(_DeviceUser);

    return ndsSuccess;
}

/**
 * @brief Register analog output channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered sucessfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registeres a #sis8300AOChannelGroup named #SIS8300NDS_AOCG_NAME
 * and then registeres #SIS8300DRV_NUM_AO_CHANNELS times #sis8300AOChannel
 * with the channel group.
 */
ndsStatus sis8300Device::registerAOChannels() {
    int                     iter;
    ndsStatus               status;
    sis8300AOChannelGroup   *cgAO;

    cgAO = new sis8300AOChannelGroup(SIS8300NDS_AOCG_NAME);
    status = registerChannelGroup(cgAO);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300NDS_AOCG_NAME);
        return ndsError;
    }

    for (iter = 0; iter < SIS8300DRV_NUM_AO_CHANNELS; iter++) {
        status = cgAO->registerChannel(new sis8300AOChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", SIS8300NDS_AOCG_NAME, iter);
            return ndsError;
        }
    }

    cgAO->setDeviceUser(_DeviceUser);

    return ndsSuccess;
}


/**
 * @brief Register frontpanel trigger channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered sucessfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registeres a base NDS channel group named #SIS8300NDS_FPCG_NAME
 * and then registeres #SIS8300DRV_NUM_FP_TRG times base NDS channel
 * with the channel group.
 */
ndsStatus sis8300Device::registerFrontTriggerChannels() {
    int                     iter;
    ndsStatus               status;
    nds::ChannelGroup       *cg;

    cg = new nds::ChannelGroup(SIS8300NDS_FPCG_NAME);
    status = registerChannelGroup(cg);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300NDS_FPCG_NAME);
        return ndsError;
    }

    for (iter = 0; iter < SIS8300DRV_NUM_FP_TRG; iter++) {
        status = cg->registerChannel(new nds::Channel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", SIS8300NDS_FPCG_NAME, iter);
            return ndsError;
        }
    }

    return ndsSuccess;
}


/**
 * @brief Register backplane trigger channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered sucessfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registeres a base NDS channel group named #SIS8300NDS_BPCG_NAME
 * and then registeres #SIS8300DRV_NUM_BP_TRG times base NDS channel
 * with the channel group.
 */
ndsStatus sis8300Device::registerBackTriggerChannels() {
    int                     iter;
    ndsStatus               status;
    nds::ChannelGroup       *cg;

    cg = new nds::ChannelGroup(SIS8300NDS_BPCG_NAME);
    status = registerChannelGroup(cg);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300NDS_BPCG_NAME);
        return ndsError;
    }

    for (iter = 0; iter < SIS8300DRV_NUM_BP_TRG; iter++) {
        status = cg->registerChannel(new nds::Channel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", SIS8300NDS_BPCG_NAME, iter);
            return ndsError;
        }
    }

    return ndsSuccess;
}


/**
 * @brief Register device register channel group.
 *
 * @retval ndsSuccess Channel group or channels registered sucessfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registeres a base NDS channel group named #SIS8300NDS_REGCG_NAME. This
 * channel group extends nds::AutoChannelGroup so channels are added
 * automatically as for all loaded records. It gives access to device registers.
 */
ndsStatus sis8300Device::registerRegisterChannels() {
    ndsStatus                   status;
    sis8300RegisterChannelGroup *cgReg;

    cgReg = new sis8300RegisterChannelGroup(SIS8300NDS_REGCG_NAME);
    status = registerChannelGroup(cgReg);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300NDS_REGCG_NAME);
        return ndsError;
    }

    cgReg->setDeviceUser(_DeviceUser);

    return ndsSuccess;
}


/**
 * @brief State handler for transition from OFF to INIT.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError Failed to open device or read info.
 *
 * Open device and read serial number and firmware version.
 */
ndsStatus sis8300Device::onSwitchInit(nds::DeviceStates from, nds::DeviceStates to) {
    int         status;
    char        buffer[SIS8300NDS_STRING_BUFFER_SIZE];
    epicsUInt32 value;

    NDS_TRC("%s", __func__);

    /* This will happen if createStructure failed. Check if the device
     * can be put into ERROR directly at the point of failure.*/
    if (!_DeviceUser) {
        NDS_ERR("Driver initialization failed.");
        return ndsError;
    }

    /* Try to open the device node specified at driver init. If this fails
     * put the device into ERROR. */
    NDS_DBG("Calling sis8300drv_open_device with %s.", _DeviceUser->file);
    status = sis8300drv_open_device(_DeviceUser);
    SIS8300NDS_STATUS_CHECK("sis8300drv_open_device", status);

    /* Read fw/serial info from card and update records. */
    status = sis8300drv_get_serial(_DeviceUser, &value);
    SIS8300NDS_STATUS_CHECK("sis8300drv_get_serial", status);
    NDS_DBG("sis8300drv_get_serial returned %u.", value);

    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%u", value);
    _serial = buffer;
    doCallbacksOctetStr(_serial, ASYN_EOM_END, _interruptIdSerial);

    status = sis8300drv_get_fw_version(_DeviceUser, &value);
    SIS8300NDS_STATUS_CHECK("sis8300drv_get_fw_version", status);
    NDS_DBG("sis8300drv_get_fw_version returned 0x%X.", value);

    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%X", value & 0xFFFF);
    _firmwareVersion = buffer;
    doCallbacksOctetStr(_firmwareVersion, ASYN_EOM_END, _interruptIdFirmware);

    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%X", value >> 16);
    _model = buffer;
    doCallbacksOctetStr(_model, ASYN_EOM_END, _interruptIdModel, _portAddr);

    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%u", SIS8300NDS_HW_REVISION);
    _hardwareRevision = buffer;
    doCallbacksOctetStr(_hardwareRevision, ASYN_EOM_END, _interruptIdHardware);

    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%.1f", SIS8300NDS_SW_VERSION);
    _softwareVersion = buffer;
    doCallbacksOctetStr(_softwareVersion, ASYN_EOM_END, _interruptIdSoftware);

    return ndsSuccess;
}


/**
 * @brief State handler for transition into INIT.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError ADC or DAC initialization failed.
 *
 * Initialize ADCs and DACs. If either failes put the device into ERROR state.
 * 
 * Push the device context to all the channel groups. In case of the AI channel
 * group the number of samples is checked if it is valid. If it is not then it
 * rounded to the nearest acceptable number.
 * 
 * Commit all parameter values of the AI CG and AI CHs to hardware. If any of
 * those fail the respective object will be put into ERROR state which will
 * also cause the device to be put into ERROR state.
 */
ndsStatus sis8300Device::onEnterInit(nds::DeviceStates from, nds::DeviceStates to) {
    int                     status;
    ndsStatus               statusSamplesCount;
    epicsInt32              samplesCountMax;
    nds::ChannelGroup       *cg;
    sis8300AIChannelGroup   *cgAI;

    NDS_TRC("%s", __func__);
    NDS_INF("entering onEnterInit"); 
    NDS_INF("Device in state %d from state %d", from, to);

    /* Initialize ADCs. */
    NDS_DBG("Calling sis8300drv_init_adc");
    status = sis8300drv_init_adc(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_init_adc", status);

    /* Initialize DACs. */
    NDS_DBG("Calling sis8300drv_init_dac");
    status = sis8300drv_init_dac(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_init_dac", status);

    cg = NULL;
    getChannelGroup(SIS8300NDS_AICG_NAME, &cg);
    if (cg != NULL) {
        NDS_DBG("Initializing channel group %s.", SIS8300NDS_AICG_NAME);
        cgAI = dynamic_cast<sis8300AIChannelGroup *>(cg);
        statusSamplesCount = cgAI->checkSamplesConfig(-1, -1, &samplesCountMax);
        if (statusSamplesCount != ndsSuccess) {
            cgAI->setSamplesCount(NULL, samplesCountMax);
        }
	NDS_INF("markAllParametersChanged");
        cgAI->markAllParametersChanged();
	NDS_INF("commitParameters");
        cgAI->commitParameters();
	NDS_INF("commitAllChannels");
        cgAI->commitAllChannels();
    }

    return ndsSuccess;
}

/*
 * This method restore the values to the registers.
 * It is supposed to be used after Reset state to restore old values
 * to registers
 * */

ndsStatus sis8300Device::restoreAllValues() {
	int status;
	// Restore DC Offset values
    NDS_DBG("Restoring old values to DC Offset");
	// Rewrite DC Offset values
    NDS_DBG("Setting VMout DC offset, I=%d, Q=%d", _VM_DC_Offset_I, _VM_DC_Offset_Q);
    status = sis8300drv_rtm_vm_set_dc_offsets(_DeviceUser, _VM_DC_Offset_I, _VM_DC_Offset_Q);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_rtm_vm_set_dc_Offset", status);

	return ndsSuccess;
}


/**
 * @brief Enable fast init
 *
 * The function is used instead of @see enableFastInit from nds
 * to allow children to override this.
 */
ndsStatus sis8300Device::fastInit(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);
    return on();
}


/**
 * @brief State handler for transition to OFF.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * After IOC init update records with currently set values otherwise close device.
 */
ndsStatus sis8300Device::onEnterOffState(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    if (from == nds::DEVICE_STATE_IOC_INIT) {
        doCallbacksInt32(_isEnabled, _interruptIdEnabled);
    } else {
        NDS_DBG("Calling sis8300drv_close_device");
        sis8300drv_close_device(_DeviceUser);
    }

    return ndsSuccess;
}

/**
 * @brief Set type of RTM used.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value RTM type.
 *
 * @retval ndsSuccess Successfully set.
 * @retval ndsError Device is in INIT state, invalid RTM type or error in
 * communication with the device.
 *
 * Valid values are enumerated in #sis8300drv_rtm from sis8300drv.h. The RTM
 * type can only be changed when device is in INIT or OFF.
 */
ndsStatus sis8300Device::setRTMType(asynUser *pasynUser, epicsInt32 value) {
    sis8300drv_rtm          rtm_type;
    nds::ChannelGroup       *cg;
    sis8300AIChannelGroup   *cgAI;

    NDS_TRC("%s", __func__);

    if (getCurrentState() != nds::DEVICE_STATE_INIT &&
            getCurrentState() != nds::DEVICE_STATE_OFF) {
        NDS_ERR("RTM can only be selected when device is in INIT or OFF.");
        return ndsError;
    }

    rtm_type = (sis8300drv_rtm)value;

    switch (rtm_type) {
        case rtm_none:
        case rtm_sis8900:
        case rtm_ds8vm1:
        case rtm_dwc8vm1:
			NDS_DBG("Setting RTM = %d", (int)rtm_type);
			break;
        default:
            NDS_ERR("Invalid RTM type.");
            return ndsError;
    }

    _RTMType = value;
    
    getChannelGroup(SIS8300NDS_AICG_NAME, &cg);
    cgAI = dynamic_cast<sis8300AIChannelGroup *>(cg);
    cgAI->resetAllAttenuators();

    doCallbacksInt32(_RTMType, _interruptIdRTMType);
    return ndsSuccess;
}


/**
 * @brief Get type of RTM used.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value RTM type.
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300Device::getRTMType(asynUser *pasynUSer, epicsInt32 *value) {
    NDS_TRC("%s", __func__);

    /* Prevent overwriting of VAL value at IOC init. */
    if (getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        return ndsError;
    }

    *value = _RTMType;
    return ndsSuccess;
}


/**
 * @brief Enable/disable vector modulator output.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Enable -1, disable - 0.
 *
 * @retval ndsSuccess Successfully set.
 * @retval ndsError Incompatible RTM type or error in
 * communication with the device.
 *
 * This can only be changed when device is in INIT or OFF and for RTM types
 * that have a vector modulator at all.
 */
ndsStatus sis8300Device::setVMOutputEnabled(asynUser *pasynUser, epicsInt32 value) {
    int             status;
    unsigned        enable;
    sis8300drv_rtm  rtm_type;

    NDS_TRC("%s", __func__);

    if (_MainInstance) {
        rtm_type = (sis8300drv_rtm)_RTMType;

        if (rtm_type != rtm_dwc8vm1 && rtm_type != rtm_ds8vm1) {
            NDS_ERR("VM output enabled cannot be modified on this RTM type.");
            return ndsError;
        }

        enable = (unsigned)!!value;

        status = sis8300drv_rtm_vm_output_enable(_DeviceUser, rtm_type, enable);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_rtm_vm_output_enable", status);
        NDS_DBG("Set VM output to %s", enable ? "enabled" : "disabled");

        _VMOutputEnable = !!value;

        doCallbacksInt32(_VMOutputEnable, _interruptIdVMOutputEnable);
    } else {
        //update the PV to disabled if the is not the main instance
        _VMOutputEnable = 0;
        doCallbacksInt32(_VMOutputEnable, _interruptIdVMOutputEnable);
        NDS_ERR("This instance is secondary, the VM output cannot be enabled");
        return ndsError;
    }

    return ndsSuccess;
}

ndsStatus sis8300Device::setVM_DC_Offset_I(asynUser *pasynUser, epicsInt32 value) {
    int         status;

    NDS_TRC("%s",__func__);
    _VM_DC_Offset_I = value;

    NDS_DBG("Setting VMout DC offset, I=%d, Q=%d", _VM_DC_Offset_I, _VM_DC_Offset_Q);

    status = sis8300drv_rtm_vm_set_dc_offsets(_DeviceUser, _VM_DC_Offset_I, _VM_DC_Offset_Q);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_rtm_vm_set_dc_Offset", status);

    doCallbacksInt32(_VM_DC_Offset_I, _interruptIdVM_DC_Offset_I);
    return ndsSuccess; 
}

ndsStatus sis8300Device::setVM_DC_Offset_Q(asynUser *pasynUser, epicsInt32 value) {
    int         status;

    NDS_TRC("%s", __func__);
    _VM_DC_Offset_Q = value;

    status = sis8300drv_rtm_vm_set_dc_offsets(_DeviceUser, _VM_DC_Offset_I, _VM_DC_Offset_Q);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_rtm_vm_set_dc_Offset", status);
    
    doCallbacksInt32(_VM_DC_Offset_Q, _interruptIdVM_DC_Offset_Q);
    return ndsSuccess;
}

/**
 * @brief Get enable/disable state of vector modulator output.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Enable -1, disable - 0.
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300Device::getVMOutputEnabled(asynUser *pasynUSer, epicsInt32 *value) {
    /* Prevent overwriting of VAL value at IOC init. */
    if (getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        return ndsError;
    }

    *value = _VMOutputEnable;
    return ndsSuccess;
}


/**
 * @brief Get RTM ADC Temperature
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value RTM ADC Temperature (Celsius)
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300Device::getRTMADCTemp(asynUser *pasynUSer, epicsInt32 *value) {
    int status;
    
	status = sis8300drv_rtm_read_info(_DeviceUser, adc_temp, value);
	SIS8300NDS_STATUS_CHECK("sis8300drv_rtm_read_info", status);

    doCallbacksInt32(*value, _interruptIdRTMADCTemp);

    return ndsSuccess;
}

/**
 * @brief Get RTM LO Level
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value RTM LO Level (dBM)
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300Device::getRTMLOLevel(asynUser *pasynUSer, epicsInt32 *value) {
    int status;
    
	status = sis8300drv_rtm_read_info(_DeviceUser, lo_level, value);
	SIS8300NDS_STATUS_CHECK("sis8300drv_rtm_read_info", status);

    doCallbacksInt32(*value, _interruptIdRTMLOLevel);

    return ndsSuccess;
}

/**
 * @brief Get RTM LO Meter Temp
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value RTM LO Meter Temp (Celsius))
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300Device::getRTMLOMeterTemp(asynUser *pasynUSer, epicsInt32 *value) {
    int status;
    
	status = sis8300drv_rtm_read_info(_DeviceUser, lo_meter_temp, value);
	SIS8300NDS_STATUS_CHECK("sis8300drv_rtm_read_info", status);

    doCallbacksInt32(*value, _interruptIdRTMLOMeterTemp);

    return ndsSuccess;
}

/**
 * @brief Get RTM VM Output Level
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value RTM VM Output Level (dBM)
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300Device::getRTMVMOutLevel(asynUser *pasynUSer, epicsInt32 *value) {
    int status;
    
	status = sis8300drv_rtm_read_info(_DeviceUser, vm_level, value);
	SIS8300NDS_STATUS_CHECK("sis8300drv_rtm_read_info", status);

    doCallbacksInt32(*value, _interruptIdRTMVMOutLevel);

    return ndsSuccess;
}

/**
 * @brief Get RTM LO VM Output Meter Temp
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value RTM VM Output Meter Temp (Celsius))
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300Device::getRTMVMOutMeterTemp(asynUser *pasynUSer, epicsInt32 *value) {
    int status;
    
	status = sis8300drv_rtm_read_info(_DeviceUser, vm_meter_temp, value);
	SIS8300NDS_STATUS_CHECK("sis8300drv_rtm_read_info", status);

    doCallbacksInt32(*value, _interruptIdRTMVMOutMeterTemp);

    return ndsSuccess;
}
