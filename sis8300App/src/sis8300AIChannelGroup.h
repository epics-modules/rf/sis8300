/**
 * @file sis8300AIChannelGroup.h
 * @brief Header file defining the sis8300 card analog input channel group class.
 * @author kstrnisa
 * @date 20.8.2013
 */

#ifndef _sis8300AIChannelGroup_h
#define _sis8300AIChannelGroup_h

#include "ndsChannelGroup.h"
#include "ndsTaskManager.h"
#include "ndsThreadTask.h"


/**
 * @brief sis8300 specific nds::ChannelGroup class that support common AI functionality.
 */
class sis8300AIChannelGroup : public nds::ChannelGroup {

public:
    sis8300AIChannelGroup(const std::string& name);
    virtual ~sis8300AIChannelGroup();

    ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    /* Parameters implemented from base class(es). */
    virtual ndsStatus setClockSource(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setClockFrequency(asynUser* pasynUser, epicsFloat64 value);
    virtual ndsStatus setClockDivisor(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setSamplesCount(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setTrigger(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);
    virtual ndsStatus onTriggerConditionParsed(asynUser *pasynUser, const nds::Trigger& trigger);
    virtual ndsStatus setTriggerDelay(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setTriggerRepeat(asynUser* pasynUser, epicsInt32 value);

    virtual ndsStatus commitParameters();
    virtual ndsStatus markAllParametersChanged();
    virtual ndsStatus commitAllChannels();
    virtual ndsStatus resetAllAttenuators();

    ndsStatus setDeviceUser(sis8300drv_usr *newDeviceUser);
    ndsStatus checkSamplesConfig(epicsInt32 nsamples, epicsInt32 nchannels, epicsInt32 *nsamplesMax);
    int isDaqFinished();
    int getEnabledChannels();

protected:
    sis8300drv_usr  *_DeviceUser;               /**< User context for sis8300drv, set by NDS Device. */
    nds::ThreadTask *_DaqTask;                  /**< NDS task/thread that waits for acquisition to finish. */
    int             _DaqFinished;               /**< Flag signifying that the acquisition finished completely. >*/
    epicsInt32      _TriggerRepeatRemaining;    /**< Remaining number of triggers to accept. */

    epicsFloat64        _PerfTiming;
    int                 _interruptIdPerfTiming;
    static std::string  PV_REASON_PERFTIMING;
    epicsFloat64        timemsec();

    std::string _TriggerConditionString;

    /* Flags to signify whether a parameter has been changed. */
    epicsInt32  _ClockSourceChanged;
    epicsInt32  _ClockFrequencyChanged;
    epicsInt32  _ClockDivisorChanged;
    epicsInt32  _SamplesCountChanged;
    epicsInt32  _TriggerConditionChanged;
    epicsInt32  _TriggerDelayChanged;
    epicsInt32  _TriggerRepeatChanged;

    virtual ndsStatus onSwitchProcessing(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);

    virtual ndsStatus handleStartMsg(asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleStopMsg(asynUser *pasynUser, const nds::Message& value);

    ndsStatus daqTask(nds::TaskServiceBase &service);
};


#endif /* _sis8300AIChannelGroup_h */
